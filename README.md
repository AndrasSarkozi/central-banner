
---

## Responsive Carousel

Carousel beállításai:
1. 228x190px  dobozban helyezkednek el. Az adform redetileg 270x225 képméretet alkalmazott.
2. A méreteket és egyéb beállításokat az index.html Carousel konstruktorában tudod elvégezni.(javasolt képméret: 270x225px).
3. horizontális és vertikális megjelenés közül választhatsz.
4. Responsive, Mobil böngészőkön a vertikális mód lép életbe.
5. autoRotationEnabled true-val lehet auto üzemmódra állítani.
6. A navigációs gombok, háttér és egyéb dizájn módosításokat a style.css-ben a fájl elején találod.
7. A feltöltött képekre bind-olást kell elhelyezni, hogy kattinthatóak legyenek.


---
